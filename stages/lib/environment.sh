#!/usr/bin/env bash
# vim: set sw=4 sts=4 ts=4 et tw=80 :

setup_environment() {
    cat <<EOF >> /etc/env.d/00basic
PATH=/opt/bin
LDPATH=/usr/local/lib
EOF
    cat <<EOF >> /etc/profile
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/${TARGET}/bin
export PATH
EOF

    eclectic env update
    source /etc/profile
}
