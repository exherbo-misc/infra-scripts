#!/bin/bash

# write_default_configs <config directory>
#
# Creates the following config files
#  - bashrc
#     - CFLAGS/CXXFLAGS for native host and cross hosts from
#       arbor/profiles/make.defaults
#  - general.conf
#     - default `world` location
#  - licences.conf
#     - default config accepting all licences
#  - options.conf
#     - build_options: jobs=2
#     - targets: <native> <cross hosts>
#  - platforms.conf
#     - PLATFORM ~PLATFORM
#  - repository_defaults.conf
#     - empty
#
write_default_configs() {
    local config_dir=${1:-/etc/paludis}

    # bashrc
    # populate from arbor/profiles/make.defaults
    cat <<EOF > ${config_dir}/bashrc
CHOST="${TARGET}"
$(. /var/db/paludis/repositories/arbor/profiles/make.defaults
declare -p | sed -r '/_(C|CXX)FLAGS=/!d; s/.*-- //' | while read line;do
    [[ "$line" == ${CLEAN_TARGET}_* ]] && echo "$line"
done
)
EOF

    # general.conf
    cat <<'EOF' > ${config_dir}/general.conf
world = ${root}/var/db/paludis/repositories/installed/world
EOF

    # licences.conf
    cat <<'EOF' > ${config_dir}/licences.conf
*/* *
EOF

    # options.conf
    cat <<'EOF' > ${config_dir}/options.conf
*/* build_options: jobs=2
EOF

    local targets="${TARGET}"

    echo '*/* targets: '"${targets}" >> ${config_dir}/options.conf

    # platforms.conf
    echo '*/* '"${PLATFORM} ~${PLATFORM}" > ${config_dir}/platforms.conf

    # repository_defaults.conf
    touch ${config_dir}/repository_defaults.conf
}

# extend_file_from_config <config dir> <config file>
#
# Extend a configuration file with the settings from the common and the
# explicit config
#
# If they specified config file is present in a config directory it will be
# appended to the target config file
#
extend_file_from_config() {
    local config_dir=${1:-/etc/paludis}
    local config_file=${2}

    local config_dirs=(
        ${CONFIG_COMMON_DIRECTORY}
        ${CONFIG_DIRECTORY}
    )

    for dir in "${config_dirs[@]}"; do
        if [[ -f ${dir}/${config_file} ]]; then
            cat ${dir}/${config_file} >> ${config_dir}/${config_file}
        fi
    done
}

# modify_for_build <config directory>
#
# Modifies the following config files
#  - bashrc
#     - CFLAGS/CXXFLAGS for native and cross hosts from `TARGET_<triple>_*`
#     - MAKEOPTS
#  - options.conf
#     - build_options: jobs=<job option>
#  - extended from config directories
#     - options.conf
#       - disabled tests via tests.bash
#     - package_mask.conf
#
modify_for_build() {
    local config_dir=${1:-/etc/paludis}

    # bashrc
    # overwrite compiler flags with the ones from config
    cat <<EOF >> ${config_dir}/bashrc
##### delete in stages #####
$(for v in $(echo ${!TARGET_*});do
    [[ "${v}" == *_@(CFLAGS|CXXFLAGS) ]] && echo "${v##TARGET_}=\"${!v}\""
done)
MAKEOPTS="-l5"
EOF

    # options.conf
    # configure the number of build jobs
    cat <<EOF >> ${config_dir}/options.conf
##### delete in stages #####
*/* build_options: jobs=${JOBS}
EOF

    # Config specific settings (options/masks/etc.)
    extend_file_from_config ${config_dir} options.conf
    extend_file_from_config ${config_dir} package_mask.conf

    # write config entries for disabled tests
    source ${CONFIG_COMMON_DIRECTORY}/tests.bash
    local cpn
    echo "" >> ${config_dir}/options.conf
    echo "# Disabled tests" >> ${config_dir}/options.conf
    for cpn in "${CPN_SKIP_TESTS[@]}"; do
        echo "${cpn} build_options: -recommended_tests"
    done >> ${config_dir}/options.conf

    target_specific_dirs=(
        ${TARGET_ARCH}
        ${TARGET_ARCH}/${TARGET_OS}
        ${TARGET_ARCH}/${TARGET_OS}/${TARGET_ABI}
    )
    for dir in "${target_specific_dirs[@]}"; do
        [[ -f ${CONFIG_BASE_DIRECTORY}/targets/${dir}/tests.bash ]] || continue

        echo "Reading disabled tests from ${CONFIG_BASE_DIRECTORY}/targets/${dir}/tests.bash"

        CPN_SKIP_TESTS=()
        source ${CONFIG_BASE_DIRECTORY}/targets/${dir}/tests.bash
        echo "" >> ${config_dir}/options.conf
        echo "# Target-specific disabled tests (${dir})" >> ${config_dir}/options.conf
        for cpn in "${CPN_SKIP_TESTS[@]}"; do
            echo "${cpn} build_options: -recommended_tests"
        done >> ${config_dir}/options.conf
    done
}

# write_configs <config directory>
#
# Creates default configs and modifies them according to the build config
#
write_configs() {
    local config_dir=${1:-/etc/paludis}

    # Create default configs (the ones that we finally ship)
    echo "Writing default configs to '${config_dir}'"
    write_default_configs ${config_dir}

    # Adjust config according to build config
    echo "Modifying configs in '${config_dir}' for build"
    modify_for_build ${config_dir}
}

# write_arbor_config <config dir> <profile>
#
# Write config file for ::arbor
#
# Writes a default arbor config using the specified profile
#
write_arbor_config() {
    local config_dir=${1}
    local profile=${2}

    cat <<EOF > ${config_dir}/repositories/arbor.conf
format = e
location = \${root}/var/db/paludis/repositories/arbor
sync = git+https://gitlab.exherbo.org/exherbo/arbor.git
profiles = \${location}/profiles/${profile}
names_cache = \${root}/var/cache/paludis/names
write_cache = \${root}/var/cache/paludis/metadata
EOF
}

# write_slash_repository_config <config directory>
#
# Write config file for ::slash repository for chroot
#
write_slash_repository_config() {
    # FIXME: /etc/paludis is bad default
    local config_dir=${1:-/etc/paludis}

    cat <<EOF >> ${config_dir}/repositories/slash.conf
##### delete in stages #####
name = slash
root = /
location = \${root}/var/db/paludis/repositories/installed
format = exndbam
names_cache = \${root}/var/cache/paludis/names
split_debug_location = /usr/${TARGET}/lib/debug
tool_prefix = ${TARGET}-
EOF
}

# write_accounts_repository_configs <config directory>
#
# Write configs for ::accounts and ::installed_accounts
write_accounts_repository_configs() {
    local config_dir=${1:-/etc/paludis}

    cat <<'EOF' > ${config_dir}/repositories/accounts.conf
format = accounts
EOF

    cat <<'EOF' > ${config_dir}/repositories/installed_accounts.conf
format = installed-accounts
handler = passwd
EOF

}

# write_installed_repository_config <config directory>
#
# Write ::installed config
write_installed_repository_config() {
    local config_dir=${1:-/etc/paludis}

    cat <<'EOF' > ${config_dir}/repositories/installed.conf
format = exndbam
location = ${root}/var/db/paludis/repositories/installed
names_cache = ${root}/var/cache/paludis/names
EOF

    cat <<EOF >> ${config_dir}/repositories/installed.conf
split_debug_location = /usr/${TARGET}/lib/debug
tool_prefix = ${TARGET}-
EOF
}

# write_special_repository_configs
#
# Write config files for the following repositories:
# - unpackaged
# - unavailable
# - graveyard
# - unavailable-unofficial
# - unwritten
# - r^2:
#   - repository
#   - repository.template
#
write_special_repository_configs() {
    local config_dir=${1:-/etc/paludis}

    # repository configs
    cat <<'EOF' > ${config_dir}/repositories/unpackaged.conf
format = installed_unpackaged
name = installed_unpackaged
location = ${root}/var/db/paludis/repositories/unpackaged
EOF
    cat <<'EOF' > ${config_dir}/repositories/unavailable.conf
format = unavailable
name = unavailable
location = ${root}/var/db/paludis/repositories/unavailable
sync = tar+https://unavailable.exherbolinux.org/exherbo_repositories.tar.bz2
importance = -100
EOF
    cat <<'EOF' > ${config_dir}/repositories/graveyard.conf
format = unwritten
name = graveyard
location = /var/db/paludis/repositories/graveyard
sync = git+https://gitlab.exherbo.org/exherbo-misc/graveyard.git
importance = -90
EOF
    cat <<'EOF' > ${config_dir}/repositories/unavailable-unofficial.conf
format = unavailable
name = unavailable-unofficial
location = ${root}/var/db/paludis/repositories/unavailable-unofficial
sync = tar+https://unavailable.exherbolinux.org/exherbo_unofficial_repositories.tar.bz2
importance = -100
EOF
    cat <<'EOF' > ${config_dir}/repositories/unwritten.conf
format = unwritten
name = unwritten
location = ${root}/var/db/paludis/repositories/unwritten
sync = git+https://gitlab.exherbo.org/exherbo-misc/unwritten.git
importance = -100
EOF

    # RepositoryRepository
    cat <<'EOF' > ${config_dir}/repositories/repository.conf
format = repository
config_filename = /etc/paludis/repositories/%{repository_template_name}.conf
config_template = /etc/paludis/repository.template
EOF
    cat <<'EOF' > ${config_dir}/repository.template
format = %{repository_template_format}
location = /var/db/paludis/repositories/%{repository_template_name}
sync = %{repository_template_sync}
EOF
}

write_paludis_configs_for_build() {
    local config_dir=${1}

    write_configs ${config_dir}

    write_installed_repository_config ${config_dir}
    write_accounts_repository_configs ${config_dir}
    write_special_repository_configs ${config_dir}

    write_arbor_config ${config_dir} ${PROFILE}
}

write_paludis_configs_for_deploy() {
    local config_dir=${1}

    # remove our distfiles mirror
    edo rm -fv ${config_dir}/mirrors.conf

    write_default_configs ${config_dir}

    write_installed_repository_config ${config_dir}
    write_accounts_repository_configs ${config_dir}
    write_special_repository_configs ${config_dir}

    write_arbor_config ${config_dir} ${PROFILE}
}

write_paludis_configs() {
    local env=${1}
    local config_dir=${2}

    edo rm -frv ${config_dir}
    edo mkdir -v ${config_dir}/{,repositories}

    case "${env}" in
        host)
            write_paludis_configs_for_build ${config_dir}
            ;;

        chroot)
            write_paludis_configs_for_build ${config_dir}
            ;;

        stage)
            write_paludis_configs_for_deploy ${config_dir}
            ;;

    esac
}

# connect_chroot_to_slash <config dir> <chroot dir>
#
# Makes the following modifications:
#  - general.conf
#     - `root` option pointing to chroot directory
#  - installed_accounts.conf
#     - `root` option pointing to /
connect_chroot_to_slash() {
    local config_dir=${1}
    local chroot_dir=${2}

    echo "Configuring chroot to '${chroot_dir}'"

    # general.conf
    # Set the `root` option appropriately
    cat <<EOF >> ${config_dir}/general.conf
##### delete in stages #####
root = ${chroot_dir}
world = \${root}/var/db/paludis/repositories/installed/world
EOF

    # installed_accounts.conf
    # Use accounts from slash
    cat <<'EOF' >> ${config_dir}/repositories/installed_accounts.conf
##### delete in stages #####
root = /
EOF

    # ::slash repository
    write_slash_repository_config ${config_dir}
}

# disconnect_chroot_from_slash <config directory>
#
# Disconnect chroot from host by removing the chroot specific settings from the
# config, which are:
#  - general.conf
#    Remove `root` and `world` pointing to chroot directory
#  - installed_accounts.conf
#    Remove `root = /`, which is required during the build process to have
#    accounts provided from build host
#  - repositories/slash.conf
#    Remove completely, it's only necessary when populating the chroot
#
disconnect_chroot_from_slash() {
    local config_dir=${1}

    for c in ${config_dir}/{general.conf,repositories/installed_accounts.conf}; do
        edo sed -n -e '/#####/,$ p' "${c}"
        edo sed -i -e '/#####/,$ d' "${c}"
    done

    edo rm -v ${config_dir}/repositories/slash.conf
}

# write_postinst_actions
write_postinst_actions() {
    # I checked for necessary action by grep'ing in arbor:
    # grep -Hr '${ROOT} == ' *
    # Then I weeded out what's not part of our stages.

    echo ${CHROOT_DIR}

    cat <<'EOF' > "${CHROOT_DIR}"/tmp/postinst_actions
# sys-apps/shadow
grpck -r
grpconv

# app-misc/ca-certificates
update-ca-certificates
EOF
}

# vim: set et ts=4 foldmethod=marker foldmarker={{{,}}} :
