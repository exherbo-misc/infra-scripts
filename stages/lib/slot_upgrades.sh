
_toolchain_update_gcc() {
    # Check if we have to upgrade GCC
    gcc_package=sys-devel/gcc
    gcc_packages=(
        ${gcc_package}
        sys-libs/libatomic
        sys-libs/libgcc
        sys-libs/libstdc++
    )

    gcc_best_available=$(cave print-ids -M none -s install -m ${gcc_package} -f '%s\n' | tail -n1)
    gcc_used=$(eclectic gcc show)
    gcc_installed=( $(cave print-ids -s uninstall -m "${gcc_package}::installed" -f '%s ') )

    echo "Best available GCC: ${gcc_best_available}"
    echo "Installed GCC slot(s): ${gcc_installed[@]}"
    echo "Selected GCC: ${gcc_used}"

    if [[ ${gcc_best_available} != ${gcc_used} ]] || [[ "${#gcc_installed[@]}" -gt 1 ]]; then
        # Install best gcc and its runtimes
        edo cave resolve -z1 -Ks "${gcc_packages[@]/%/:${gcc_best_available}}" -x

        # Assure it's selected
        edo eclectic gcc set ${gcc_best_available}

        # Remove old GCC versions
        for slot in ${gcc_installed}; do
            [[ ${slot} == ${gcc_best_available} ]] && continue

            for p in $(cave print-set -x gcc-split); do
                cave print-ids -s uninstall -m "${p}:${slot}" -f '%I::%r\n'
            done | xargs -L1 cave perform uninstall
        done

        # Reinstall packages with `providers:libstdc++` option
        libstdcxx_dependents=$(cave print-ids -m '*/*::/[.MYOPTIONS<libstdc++]' -f '%I\n')
        if [[ -n ${libstdcxx_dependents} ]]; then
            edo cave resolve -1zx ${libstdcxx_dependents}
        fi
    fi
}

_perl_update() {
    # Check if we have to upgrade perl
    perl_package=dev-lang/perl
    perl_best_available=$(cave print-ids -M none -s install -m ${perl_package} -f '%s\n' | tail -n1)
    perl_used=$(eclectic perl show)
    perl_installed=( $(cave print-ids -s uninstall -m "${perl_package}::installed" -f '%s ') )

    echo "Best available perl: ${perl_best_available}"
    echo "Installed perl slot(s): ${perl_installed[@]}"
    echo "Selected perl: ${perl_used}"

    if [[ ${perl_best_available} != ${perl_used} ]] || [[ "${#perl_installed[@]}" -gt 1 ]]; then
        # Install best perl
        edo cave resolve -z1 -Ks "${perl_package}:${perl_best_available}" -x

        # Assure it's selected
        eclectic perl set ${perl_best_available}

        for slot in ${perl_installed}; do
            [[ ${slot} == ${perl_best_available} ]] && continue

            # NOTE: At this point we would like to use `cave resolve -D` to rebuild all
            # packages depending on the old perl version. Unfortunately that will also
            # rebuild packages having a build-only dependency on perl:* too, which is
            # unnecessarily and and can complicate getting other upgrade paths right.
            # Therefore we find those dependents manually with the following commands.

            # Reinstall all libs for new perl
            old_packages="$(for f in /usr/${TARGET}/lib/perl5/*/${slot}*; do cave print-owners --format '%I\n' $f; done | sort -u | grep -v '^dev-lang/perl:')"
            if [[ -n ${old_packages} ]]; then
                edo cave resolve -1zx -si ${old_packages}
            fi

            # Fix files which still have a shebang using the old perl
            for f in /usr/${TARGET}/bin/*; do
                head -n1 "$f" | grep -q -a "perl${slot//./\\.}" && cave print-owners --format '%I\n' "$f"
            done | sort -u | grep -v '^dev-lang/perl:' | xargs -r cave resolve -1zx -si

            # Reinstall packages that are not picked up by the above commands
            edo cave resolve -z1 sys-apps/texinfo -x

            # Uninstall old perl
            edo cave uninstall perl:${slot} -u system -x

            # Sanity check to assure old perl was uninstalled
            [[ -n $(cave print-ids -m "dev-lang/perl:${slot}" -s uninstall) ]] && die "Old perl is still there after uninstall"
        done
    fi
}

_python_update() {
    # Check if we have to upgrade python
    python_package=dev-lang/python

    # Get default python ABI from profile
    # Even if a newer non-masked python slot is available we have to stick to
    # the default ABI specified in the profile to avoid a version mismatch
    python_default_abi=$(
        cat /var/db/paludis/repositories/arbor/profiles/options.conf \
            | grep '\*/\* PYTHON_ABIS: ' \
            | sed -e 's/.*:\s\+//'
    )
    python_used=$(eclectic python3 show)
    python_installed=( $(cave print-ids -s uninstall -m "${python_package}::installed" -f '%s ') )

    echo "Default python ABI: ${python_default_abi}"
    echo "Installed python slot(s): ${python_installed[@]}"
    echo "Selected python: ${python_used}"

    if [[ ${python_default_abi} != ${python_used} ]] || [[ "${#python_installed[@]}" -gt 1 ]]; then
        # Install best python
        edo cave resolve -z1 -Ks "${python_package}:${python_default_abi}" -x

        for slot in ${python_installed}; do
            [[ ${slot} == ${python_default_abi} ]] && continue

            # NOTE(marvs): `resolve -D ${python_package}:${slot}` is too broad,
            # since it picks up build-only `python:* deps as well, which can be
            # very costly (e.g. glibc)

            # Reinstall all python packages for new python ABI
            old_packages=(
                # All packages installing a python library
                $(cave print-owners --format '%I\n' /usr/${TARGET}/lib/python${slot})

                # All packages installing python scripts
                # NOTE: Searching for python${slot//./\\.} doesn't catch asciidoc which
                # has a `#!/usr/bin/env python3` shebang
                $(for f in /usr/${TARGET}/bin/*; do
                      head -n1 "$f" | grep -q -a "python3" && cave print-owners --format '%I\n' "$f"
                  done | sort -u)

                # systemd only uses python to build its man-pages, but has a
                # (build+run) dependency on a specific Python ABI, so we need to
                # reinstall it in order to be able to remove the slot ABI
                $(cave print-ids -s uninstall -m "sys-apps/systemd::installed" -f '%I\n')

                # libxcrypt has a python dependency for its tests which causes it
                # have PYTHON_ABIS defined even without the tests being enabled and
                # that's enough to still end up with a dependency on python
                $(cave print-ids -s uninstall -m "dev-libs/libxcrypt::installed" -f '%I\n')
            )

            old_packages_filtered=$(IFS=$'\n'; echo "${old_packages[*]}" | sort -u | grep -v '^dev-lang/python:')

            if [[ -n ${old_packages_filtered} ]]; then
                edo cave resolve -1zx -si ${old_packages_filtered}
            fi

            # Uninstall old python
            edo cave uninstall dev-lang/python:${slot} -u system -x

            # Sanity check to assure old python was uninstalled
            [[ -n $(cave print-ids -m "dev-lang/python:${slot}" -s uninstall) ]] && die "Old python is still there after uninstall"
        done
    fi

    # Ensure default is selected
    eclectic python3 set ${python_default_abi}
}

_icu_update() {
    # Check if we have to upgrade icu
    icu_package=dev-libs/icu
    icu_best_available=$(cave print-ids -M none -s install -m ${icu_package} -f '%s\n' | tail -n1)
    icu_installed=( $(cave print-ids -s uninstall -m "${icu_package}::installed" -f '%s ') )
    icu_best_installed=( $(cave print-ids -s uninstall -m "${icu_package}::installed" -f '%s ' | tail -n1) )

    echo "Best available icu: ${icu_best_available}"
    echo "Installed icu slot(s): ${icu_installed[@]}"
    if [[ ${icu_best_available} != ${icu_best_installed} ]] || [[ "${#icu_installed[@]}" -gt 1 ]]; then
        # Install best icu
        edo cave resolve -z1 -Ks "${icu_package}:${icu_best_available}" -x

        for slot in ${icu_installed}; do
            [[ ${slot} == ${icu_best_available} ]] && continue

            edo cave resolve -D dev-libs/icu:${slot} \!dev-libs/icu:${slot} -x

            # Fix broken packages that didn't get picked up (e.g. clients of libxml2 using
            # autotools overlink to icu (and others) because of libtool)
            edo cave fix-linkage -x

            # Sanity check to assure old icu was uninstalled
            [[ -n $(cave print-ids -m "dev-libs/icu:${slot}" -s uninstall) ]] && die "Old icu is still there after uninstall"
        done
    fi
}

_openssl_update() {
    # Check if we have to upgrade openssl
    openssl_package=dev-libs/openssl
    openssl_best_available=$(cave print-ids -M none -s install -m ${openssl_package} -f '%s\n' | tail -n1)
    openssl_installed=( $(cave print-ids -s uninstall -m "${openssl_package}::installed" -f '%s ') )
    openssl_best_installed=( $(cave print-ids -s uninstall -m "${openssl_package}::installed" -f '%s ' | tail -n1) )

    echo "Best available openssl: ${openssl_best_available}"
    echo "Installed openssl slot(s): ${openssl_installed[@]}"
    if [[ ${openssl_best_available} != ${openssl_best_installed} ]] || [[ "${#openssl_installed[@]}" -gt 1 ]]; then
        # Install best openssl
        edo cave resolve -z1 -Ks "${openssl_package}:${openssl_best_available}" -x

        for slot in ${openssl_installed}; do
            [[ ${slot} == ${openssl_best_available} ]] && continue

            # systemd build fails if util-linux isn't rebuilt first
            edo cave resolve util-linux -1x

            # uninstall separately so we dont fail pulling distfiles if it's removed first
            edo cave resolve nothing -D dev-libs/openssl:${slot} -x
            edo cave resolve \!dev-libs/openssl:${slot} -x

            edo cave fix-linkage -x

            # Sanity check to assure old openssl was uninstalled
            [[ -n $(cave print-ids -m "dev-libs/openssl:${slot}" -s uninstall) ]] && die "Old openssl is still there after uninstall"
        done
    fi
}

_vim_runtime_update() {
    # Check if we have to upgrade vim-runtime
    vim_runtime_package=app-editors/vim-runtime
    vim_package=app-editors/vim

    vim_runtime_best_available=$(cave print-ids -M none -s install -m ${vim_runtime_package} -f '%s\n' | tail -n1)
    vim_runtime_installed=( $(cave print-ids -s uninstall -m "${vim_runtime_package}::installed" -f '%s ') )
    vim_runtime_best_installed=( $(cave print-ids -s uninstall -m "${vim_runtime_package}::installed" -f '%s ' | tail -n1) )

    echo "Best available vim-runtime: ${vim_runtime_best_available}"
    echo "Installed vim-runtime slot(s): ${vim_runtime_installed[@]}"
    if [[ ${vim_runtime_best_available} != ${vim_runtime_best_installed} ]] || [[ "${#vim_runtime_installed[@]}" -gt 1 ]]; then
        # Install best vim-runtime
        edo cave resolve -z1 -Ks "${vim_runtime_package}:${vim_runtime_best_available}" "${vim_package}" -x

        for slot in ${vim_runtime_installed}; do
            [[ ${slot} == ${vim_runtime_best_available} ]] && continue
            # uninstall separately so we dont fail pulling distfiles if it's removed first
            edo cave resolve nothing -D ${vim_runtime_package}:${slot} -x
            edo cave resolve \!${vim_runtime_package}:${slot} -x

            edo cave fix-linkage -x

            # Sanity check to assure old vim-runtime was uninstalled
            [[ -n $(cave print-ids -m "dev-libs/vim-runtime:${slot}" -s uninstall) ]] && die "Old vim-runtime is still there after uninstall"
        done
    fi
}

