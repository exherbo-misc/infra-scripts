CPN_SKIP_TESTS=(
    ### Disabled tests to avoid packages

    # pulls in packages from ::gnome and ::x11
    # last checked: 28 Jan 2019 (version )
    dev-libs/glib

    # pulls in packages from ::gnome, ::x11 and ::desktop
    # last checked: 28 Jan 2019 (version )
    gnome-desktop/dconf

    # pulls in a number of packages from ::perl
    # last checked: 20121114
    net-misc/wget

    # needs Data-Compare, Data-Dump and Test-Deep from ::perl
    # last checked: 28 Jan 2019 (version )
    sys-apps/texinfo

    # pulls in libgfortran
    sys-devel/libtool

    # pulls in stuff from ::perl for tests
    dev-scm/git

    # pulls in pytest from ::python
    dev-python/Jinja2
    dev-python/MarkupSafe

    # pulls in passlib from ::python
    dev-libs/libxcrypt

    # pulls in pytest-mock from ::python
    app-doc/asciidoc


    ### Tests that don't work in CI environment
    app-editors/vim

    # lsfd/mkfds-tcp6 and lsfd/mkfds-udp6 fail in docker
    # as ipv6 is experimental and not enabled by default
    sys-apps/util-linux


    ### Misc failures
    ## Broken

    # Fails a test in CI environment
    # Last checked: 29 Oct 2021 (version: 9.0)
    # Context:
    # FAIL: tests/tail-2/inotify-dir-recreate.sh
    #
    # Seems to be overlayfs related:
    # https://github.com/containers/podman/issues/5493#issuecomment-598851397
    # https://debbugs.gnu.org/cgi/bugreport.cgi?bug=47940
    sys-apps/coreutils


    ## Test failures

    # Tests hang / wait for input
    # Last checked: 05 Mar 2019 (version: 7.64.0)
    net-misc/curl

    # connection-gnutls test fails at on x86_64 gnu/musl
    # Last checked: 03 May 2020 (version 2.62.3)
    # Context:
    # Bail out! GLib-Net:ERROR:../glib-networking-2.62.3/tls/tests/connection.c:2035:quit_on_handshake_complete: assertion failed (error == (g-tls-error-quark, 3)): error is NULL
    dev-libs/glib-networking

    # Single test fails on all arch's
    # Last checked: 07 Feb 2023 (version 1.22.4)
    # FAIL: contrib/gdiffmk/tests/gdiffmk_tests.sh
    sys-apps/groff

    # (At least) one test hangs when running in a docker container
    # Last checked: 25 Oct 2023 (versions: 3.10.13, 3.11.6)
    # "running: test_subprocess (23 min 22 sec)"
    #
    dev-lang/python
)
