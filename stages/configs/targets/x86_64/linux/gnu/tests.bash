CPN_SKIP_TESTS+=(
    # Fails a test in CI environment
    # Last checked: 15 Aug 2022 (version: 4.0.0)
    # Context:
    # Running /var/tmp/paludis/build/sys-process-procps-4.0.0/work/procps-ng-4.0.0/testsuite/free.test/free.exp ...
    # FAIL: free with commit
    sys-process/procps
)
