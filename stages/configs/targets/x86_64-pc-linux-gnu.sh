TARGET_x86_64_pc_linux_gnu_CROSS_TARGET=
TARGET_x86_64_pc_linux_gnu_CFLAGS="-march=x86-64 -mtune=generic -pipe -O2"
TARGET_x86_64_pc_linux_gnu_CXXFLAGS="${TARGET_x86_64_pc_linux_gnu_CFLAGS}"
TARGET_x86_64_pc_linux_gnu_PLATFORM=amd64
TARGET_x86_64_pc_linux_gnu_LIBC=sys-libs/glibc
TARGET_x86_64_pc_linux_gnu_DOCKER_PLATFORM="linux/amd64"
