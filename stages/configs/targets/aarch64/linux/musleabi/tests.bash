CPN_SKIP_TESTS+=(
    # Fails some tests
    # Last checked: 02 May 2020 (version 2.9.10-r1)
    # Context:
    # ## XML regression tests
    # File ./test/icu_parse_test.xml generated an error
    # ## XML regression tests on memory
    # File ./test/icu_parse_test.xml generated an error
    # ## XML entity subst regression tests
    # File ./test/icu_parse_test.xml generated an error
    #
    # [ more icu_parse_test.xml related errors ]
    #
    # Total 3174 tests, 11 errors, 0 leaks
    dev-libs/libxml2

    # Fails a test
    # Last checked: 02 May 2020 (version 2.4.48)
    # Context:
    # [13] $ setfattr -n user -v value f -- failed
    # setfattr: f: Not supported                                               != setfattr: f: Operation not supported
    #
    # Problem:
    # musl stringifies errno ENOTSUP differently than glibc and attr's test
    # hardcodes that error string
    sys-apps/attr

    # Fails some tests
    # Last checked: 03 May 2020 (version: 5.1.0)
    # Context:
    # 3 TESTS FAILED
    # _clos1way6: (wrong output order)
    # _nonfatal3:
    #   nonfatal3.awk:4: warning: remote host and port information (localhost, 0) invalid: System error
    # _testext:
    #   ! test_errno() returned 1, ERRNO = No child processes
    #   vs.
    #   ! test_errno() returned 1, ERRNO = No child process
    sys-apps/gawk

    # Fails a test
    # last checked: 19 Sep 2021 (version: 10.37)
    # Context:
    # FAIL: RunGrepTest
    #
    #  @@ -1,22 +1,22 @@
    #  Arg1: [T] [he ] [ ] Arg2: |T| () () (0)
    # +The quick brown
    #  Arg1: [T] [his] [s] Arg2: |T| () () (0)
    #  Arg1: [T] [his] [s] Arg2: |T| () () (0)
    #  Arg1: [T] [he ] [ ] Arg2: |T| () () (0)
    #  Arg1: [T] [he ] [ ] Arg2: |T| () () (0)
    #  Arg1: [T] [he ] [ ] Arg2: |T| () () (0)
    # -The quick brown
    #  [...]
    dev-libs/pcre2

    # Fails a test
    # Last checked: 26 Apr 2022 (version: 1.4.19)
    # Context:
    # The test started failing with musl 1.2.3
    # Upstream bug: https://lists.gnu.org/archive/html/bug-gnulib/2022-04/msg00013.html
    sys-devel/m4

    # Fails a bunch of tests
    # Last checked: 10 Jun 2022 (version: 23.5)
    # Context:
    # Running ./killall.test/killall.exp ...
    # FAIL: killall using signal HUP
    # FAIL: killall using signal SIGHUP
    # FAIL: killall using signal SIGINT
    # FAIL: killall using signal QUIT
    # FAIL: killall using signal SIGQUIT
    # FAIL: killall using signal SIGILL
    # FAIL: killall using signal TRAP
    # FAIL: killall using signal SIGTRAP
    # [...]
    #
    # Upstream bug: https://gitlab.com/psmisc/psmisc/-/issues/18
    sys-process/psmisc

    # Fails a test
    # Last checked: 16 Aug 2022 (version: 1.12.1)
    # Context:
    # 98% tests passed, 1 tests failed out of 45
    #
    # The following tests FAILED:
    #          10 - googletest-port-test (Failed)
    #
    dev-cpp/gtest

    # Fails a test
    # Last checked: 06 Sep 2022 (version: 4.19.0)
    # Context:
    # FAIL: version
    # =============
    #
    # ASN1_VERSION: 4.19.0
    # ASN1_VERSION_MAJOR: 4
    # ASN1_VERSION_MINOR: 19
    # ASN1_VERSION_PATCH: 0
    # ASN1_VERSION_NUMBER: 41300
    # (ASN1_VERSION_MAJOR << 16) + (ASN1_VERSION_MINOR << 8) + ASN1_VERSION_PATCH: 41300
    # ASN1_VERSION_MAJOR.ASN1_VERSION_MINOR.ASN1_VERSION_PATCH: 4.19.0
    # asn1_check_version (NULL): 4.19.0
    # FAIL: asn1_check_version (UNKNOWN)
    # FAIL version (exit status: 1)
    #
    dev-libs/libtasn1

    # Fails a test
    # Last checked: 15 Sep 2022 (version: 5.43)
    # Context:
    # ../tests/dsd64-dsf.testfile: DSD Stream File, mono, simple-rate, 1 bit, 141184 samples
    # lt-test: ERROR: result was (len 57)
    # DSD Stream File, mono, simple-rate, 1 bit, 141184 samples
    # expected (len 94)
    # DSF audio bitstream data, 1 bit, mono, "DSD 64" 2822400 Hz, no compression, ID3 version 2.3.0
    #
    # Reported upstream: https://bugs.astron.com/view.php?id=382
    sys-apps/file

    # Fails a test
    # Last checked: 17 Sep 2022 (version: 4.8)
    # Context:
    # FAIL: test-canonicalize-lgpl
    #
    # Root cause:
    # realpath("//.", NULL) returns "//.", but it's expected to return "/"
    sys-apps/sed

    # Two tests seem to fail under docker
    dev-libs/libunistring

    # Last checked 2023-12-01 (5.38.1)
    # Failed test 42 - check that illegal startup environment falls back at run/locale.t line 619
    #      got ''
    # expected /(?^:Falling back to the standard locale)/
    dev-lang/perl

    # Last checked 2023-12-01 (2.12.0)
    # FAIL: lexgrog-basic
    # FAIL: lexgrog-backslash-dash-rhs
    # FAIL: lexgrog-multiple-whatis
    # FAIL: man-deleted-directory
    # FAIL: manconv-incomplete-char-at-eof
    # FAIL: mandb-basic
    # FAIL: manconv-guess-from-encoding
    # FAIL: manconv-odd-combinations
    # FAIL: mandb-symlink-target-timestamp
    # FAIL: mandb-purge-updates-timestamp
    # FAIL: mandb-symlink-beats-whatis-ref
    # FAIL: mandb-regular-file-symlink-changes
    # FAIL: zsoelim-so-includes
    # FAIL: mandb-whatis-broken-link-changes
    # FAIL: whatis-path-to-executable
    # FAIL: manconv-coding-tags
    # FAIL: man-recode-suffix
    # FAIL: man-recode-in-place
    # FAIL: man-executable-page-on-path
    # FAIL: man-language-specific-requests
    # FAIL: man-so-links-same-section
    sys-apps/man-db
)
