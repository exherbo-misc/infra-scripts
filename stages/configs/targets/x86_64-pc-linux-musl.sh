TARGET_x86_64_pc_linux_musl_CROSS_TARGET=
TARGET_x86_64_pc_linux_musl_CFLAGS="-march=x86-64 -mtune=generic -pipe -O2"
TARGET_x86_64_pc_linux_musl_CXXFLAGS="${TARGET_x86_64_pc_linux_musl_CFLAGS}"
TARGET_x86_64_pc_linux_musl_PLATFORM=amd64
TARGET_x86_64_pc_linux_musl_LIBC=sys-libs/musl
TARGET_x86_64_pc_linux_musl_DOCKER_PLATFORM="linux/amd64"

SANDBOXING_WORKS=false
