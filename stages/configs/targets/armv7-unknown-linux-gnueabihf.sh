TARGET_armv7_unknown_linux_gnueabihf_CFLAGS="-march=armv7-a -mfloat-abi=hard -mfpu=vfpv3-d16 -pipe -O2"
TARGET_armv7_unknown_linux_gnueabihf_CXXFLAGS="${TARGET_armv7_unknown_linux_gnueabihf_CFLAGS}"
TARGET_armv7_unknown_linux_gnueabihf_PLATFORM=armv7
TARGET_armv7_unknown_linux_gnueabihf_LIBC=sys-libs/glibc
TARGET_armv7_unknown_linux_gnueabihf_DOCKER_PLATFORM="linux/arm/v7"

SANDBOXING_WORKS=false
