#!/bin/bash

if [[ -z "$1" ]] ; then
    echo "USAGE: $0 stage_archive"
    exit 1
fi

SCRIPT_DIR="$(dirname "$0")"

ARCHIVE=$1
NAME=$(basename "${ARCHIVE}")
NAME=${NAME%.tar.xz}
DATE=$(date +%Y%m%d)

DOCKER_IMAGE_NAME=${NAME}-base
DOCKER_IMAGE_TAG=${DATE}
DOCKER_IMAGE=exherbo/${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG}
DOCKER_IMAGE_LATEST=exherbo/${DOCKER_IMAGE_NAME}:latest

set -e

source "${SCRIPT_DIR}"/../../stages/configs/targets/${HOST}.sh
v="TARGET_${HOST//-/_}_CFLAGS"
CFLAGS="${!v}"
v="TARGET_${HOST//-/_}_DOCKER_PLATFORM"
DOCKER_PLATFORM="${!v}"

echo "Importing ${HOST}-${TOOLCHAIN} base container..."
docker import --platform="${DOCKER_PLATFORM}" "${ARCHIVE}" ${DOCKER_IMAGE}
docker push ${DOCKER_IMAGE}

docker tag ${DOCKER_IMAGE} ${DOCKER_IMAGE_LATEST}
docker push ${DOCKER_IMAGE_LATEST}

if [[ ${NAME} == *"-musl"* ]] ; then
    echo "musl ci containers are not currently supported"
    exit 0
fi

echo "Building ${HOST}-${TOOLCHAIN} CI container..."
pushd "$(dirname "${BASH_SOURCE[0]}")"/ci
docker build \
    --progress plain \
    --platform "${DOCKER_PLATFORM}" \
    --build-arg HOST="${HOST}" \
    --build-arg TOOLCHAIN="${TOOLCHAIN}" \
    --build-arg CFLAGS="${CFLAGS}" \
    --build-arg STAGE_BUILD_JOBS=${STAGE_BUILD_JOBS:-5} \
    --no-cache \
    --pull \
    -t exherbo/exherbo_ci:${HOST} \
    .
docker push exherbo/exherbo_ci:${HOST}
# you can't update the existing manifest so we need to create and include
# each HOST every time
docker manifest create \
  exherbo/exherbo_ci:latest \
  --amend exherbo/exherbo_ci:aarch64-unknown-linux-gnueabi \
  --amend exherbo/exherbo_ci:armv7-unknown-linux-gnueabihf \
  --amend exherbo/exherbo_ci:x86_64-pc-linux-gnu
docker manifest push exherbo/exherbo_ci:latest
popd

echo "Building ${HOST}-${TOOLCHAIN} DID container..."
pushd "$(dirname "${BASH_SOURCE[0]}")"/did
docker build \
    --progress plain \
    --platform "${DOCKER_PLATFORM}" \
    --build-arg HOST="${HOST}" \
    --build-arg TOOLCHAIN="${TOOLCHAIN}" \
    --build-arg CFLAGS="${CFLAGS}" \
    --build-arg STAGE_BUILD_JOBS=${STAGE_BUILD_JOBS:-5} \
    --no-cache \
    --pull \
    -t exherbo/exherbo_did:${HOST} \
    .
docker push exherbo/exherbo_did:${HOST}
docker manifest create \
  exherbo/exherbo_did:latest \
  --amend exherbo/exherbo_did:aarch64-unknown-linux-gnueabi \
  --amend exherbo/exherbo_did:armv7-unknown-linux-gnueabihf \
  --amend exherbo/exherbo_did:x86_64-pc-linux-gnu
docker manifest push exherbo/exherbo_did:latest
popd
