#!/bin/sh

set -e

HOST=${1}
CFLAGS=${2}

echo "Setting up CI environment"
source /etc/profile
eclectic env update

chgrp paludisbuild /dev/tty

# make sure that we build generic binaries
cat <<EOF > /etc/paludis/bashrc
CHOST="${HOST}"
${HOST//-/_}_CFLAGS="${CFLAGS}"
${HOST//-/_}_CXXFLAGS="${CFLAGS}"
EOF

# docker build does not allow to add the SYS_PTRACE cap
export PALUDIS_DO_NOTHING_SANDBOXY=1

# set to STAGE_BUILD_JOBS if defined, or update to 5
# this needs to be forced back to 5 at the end of the build
# so the image itself has a sensible default
sed -i 's/jobs=2/jobs='${STAGE_BUILD_JOBS:-5}'/g' /etc/paludis/options.conf

# options.conf.d for temporary options
mkdir -v /etc/paludis/options.conf.d
echo '*/* build_options: -recommended_tests' >> /etc/paludis/options.conf.d/disabled-tests.conf

# mrothe+python for s3cmd and deps
cave resolve -1x repository/{mrothe,python}

echo "sys-apps/paludis ruby" >> /etc/paludis/options.conf
echo "dev-lang/python sqlite" >> /etc/paludis/options.conf

cave resolve -x \
  sys-apps/paludis \
  dev-ruby/ruby-elf \
  net-apps/s3cmd

# do a purge to remove the stage set as
# its not needed for ci
cave purge -x

# Remove temporary options
rm -rf /etc/paludis/options.conf.d/

# Restore default build jobs
sed -ri 's/jobs=[0-9]+/jobs=5/g' /etc/paludis/options.conf

echo "Downloading build scripts"
cd /usr/local/bin
wget -c https://gitlab.exherbo.org/exherbo-misc/infra-scripts/-/raw/master/continuous-integration/gitlab/buildtest
wget -c https://gitlab.exherbo.org/exherbo-misc/infra-scripts/-/raw/master/continuous-integration/gitlab/handle_confirmations
wget -c https://gitlab.exherbo.org/exherbo-misc/exherbo-dev-tools/-/raw/master/mscan2.rb
wget -c https://gitlab.exherbo.org/exherbo-misc/exherbo-dev-tools/-/raw/master/check_slots
chmod +x buildtest handle_confirmations mscan2.rb check_slots

echo "Cleaning up again"
rm -f /build.sh
rm -f /root/.bash_history
rm -Rf /tmp/*
rm -Rf /var/tmp/paludis/build/*
rm -Rf /var/cache/paludis/distfiles/*
rm -Rf /var/log/paludis/*
rm -f /var/log/paludis.log

