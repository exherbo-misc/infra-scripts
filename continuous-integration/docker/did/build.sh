#!/bin/sh

set -e

HOST=${1}
CFLAGS=${2}

echo "Setting up DID environment"
source /etc/profile
eclectic env update

chgrp paludisbuild /dev/tty

# make sure that we build generic binaries
cat <<EOF > /etc/paludis/bashrc
CHOST="${HOST}"
${HOST//-/_}_CFLAGS="${CFLAGS}"
${HOST//-/_}_CXXFLAGS="${CFLAGS}"
EOF

# docker build does not allow to add the SYS_PTRACE cap
export PALUDIS_DO_NOTHING_SANDBOXY=1

# set to STAGE_BUILD_JOBS if defined, or update to 5
# this needs to be forced back to 5 at the end of the build
# so the image itself has a sensible default
sed -i 's/jobs=2/jobs='${STAGE_BUILD_JOBS:-5}'/g' /etc/paludis/options.conf

echo '*/* build_options: -recommended_tests' >> /etc/paludis/options.conf

# python is required for btrfs-progs Sphinx dependency
cave resolve -1x \
  repository/virtualization \
  repository/net \
  repository/python

cave generate-metadata

echo 'dev-lang/go bootstrap' >> /etc/paludis/options.conf
cave resolve dev-lang/go -xz1
sed -e 's:dev-lang/go bootstrap::g' -i /etc/paludis/options.conf
cave resolve docker --permit-old-version sys-apps/tini -x
cave resolve docker-buildx -x

# do a purge to remove the stage set as
# its not needed for ci
cave purge -x

sed '/\*\/\* build_options: -recommended_tests/d' -i /etc/paludis/options.conf

# Restore default build jobs
sed -ri 's/jobs=[0-9]+/jobs=5/g' /etc/paludis/options.conf

echo "Cleaning up again"
rm -f /build.sh
rm -f /root/.bash_history
rm -Rf /tmp/*
rm -Rf /var/tmp/paludis/build/*
rm -Rf /var/cache/paludis/distfiles/*
rm -Rf /var/log/paludis/*
rm -f /var/log/paludis.log

